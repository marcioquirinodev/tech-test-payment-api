﻿using Dados.Entidades;

namespace Dados.Interfaces
{
    public interface IVendaRepositorio : IRepositorio<Venda>
    {
    }
}
