﻿using Dados.Entidades;

namespace Dados.Interfaces
{
    public interface IRepositorio<T> : IDisposable where T : Entidade
    {
        Task<bool> Adicionar(T obj);
        Task<bool> Atualizar(T obj);
        Task<T> ObterPorId(Guid id);
        Task<IEnumerable<T>> ObterTodos();
        Task<bool> Remover(Guid id);
        Task<int> SaveChanges();
    }
}
