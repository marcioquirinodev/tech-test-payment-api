﻿using Dados.Entidades;

namespace Dados.Interfaces
{
    public interface IProdutoRepositorio : IRepositorio<Produto>
    {
    }
}
