﻿using Dados.Entidades;

namespace Dados.Interfaces
{
    public interface IItemVendaRepositorio : IRepositorio<ItemVenda>
    {
    }
}
