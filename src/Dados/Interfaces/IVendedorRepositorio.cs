﻿using Dados.Entidades;

namespace Dados.Interfaces
{
    public interface IVendedorRepositorio : IRepositorio<Vendedor>
    {
    }
}
