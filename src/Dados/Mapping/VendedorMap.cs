﻿using Dados.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dados.Mapping
{
    public class VendedorMap : IEntityTypeConfiguration<Vendedor>
    {
        public void Configure(EntityTypeBuilder<Vendedor> vendedor)
        {
            vendedor.ToTable("Vendedores");
            vendedor.HasKey(pk=> pk.Id);
            vendedor.Property(x => x.CPF).HasColumnType("varchar(11)").IsRequired();
            vendedor.HasIndex(x => x.CPF).IsUnique();
            vendedor.Property(x => x.Nome).HasColumnType("varchar(150)").IsRequired();
            vendedor.Property(x => x.Email).HasColumnType("varchar(150)").IsRequired();
            vendedor.HasIndex(x => x.Email).IsUnique();
            vendedor.Property(x => x.Telefone).HasColumnType("varchar(11)").IsRequired();
        }
    }
}
