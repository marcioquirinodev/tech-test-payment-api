﻿using Dados.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dados.Mapping
{
    public class ProdutoMap : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> produto)
        {
            produto.ToTable("Produtos");
            produto.HasKey(pk => pk.Id);
            produto.Property(x => x.Descricao).HasColumnType("varchar(255)").IsRequired();
            produto.Property(x => x.Valor).HasColumnType("decimal(11,2)").IsRequired();
        }
    }
}
