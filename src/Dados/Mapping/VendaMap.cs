﻿using Dados.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dados.Mapping
{
    public class VendaMap : IEntityTypeConfiguration<Venda>
    {
        public void Configure(EntityTypeBuilder<Venda> venda)
        {
            venda.ToTable("Vendas");
            venda.HasKey(pk=>pk.Id);
            venda.Property(x => x.Data).IsRequired();
            venda.Property(x=>x.Status).IsRequired();
            venda.Property(x=>x.VendedorId).IsRequired();
            venda.HasOne(v=>v.Vendedor).WithMany(v=>v.Vendas).HasForeignKey(fk=>fk.VendedorId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
