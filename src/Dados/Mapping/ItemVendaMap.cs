﻿using Dados.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Dados.Mapping
{
    public class ItemVendaMap : IEntityTypeConfiguration<ItemVenda>
    {
        public void Configure(EntityTypeBuilder<ItemVenda> item)
        {
            item.ToTable("Itens");
            item.HasKey(pk => pk.Id);
            item.Property(x=>x.Quantidade).IsRequired();
            item.Property(x => x.VendaId).IsRequired();
            item.Property(x => x.ProdutoId).IsRequired();
            item.HasOne(v=>v.Venda).WithMany(v=>v.Itens).HasForeignKey(fk=>fk.VendaId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
