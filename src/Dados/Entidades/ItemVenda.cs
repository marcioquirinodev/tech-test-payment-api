﻿namespace Dados.Entidades
{
    public class ItemVenda : Entidade
    {
        public int Quantidade { get; set; }
        public Guid VendaId { get; set; }
        public Guid ProdutoId { get; set; }
        public Venda Venda { get; set; }
        public Produto Produto { get; set; }
    }
}
