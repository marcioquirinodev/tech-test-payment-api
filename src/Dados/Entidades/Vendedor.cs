﻿namespace Dados.Entidades
{
    public class Vendedor : Entidade
    {
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public IEnumerable<Venda> Vendas { get; set; }
    }
}
