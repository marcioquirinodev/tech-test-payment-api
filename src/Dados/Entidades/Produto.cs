﻿namespace Dados.Entidades
{
    public class Produto : Entidade
    {
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
    }
}
