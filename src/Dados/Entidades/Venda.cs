﻿using Dados.Enum;

namespace Dados.Entidades
{
    public class Venda : Entidade
    {
        public DateTime Data { get; set; }
        public Status Status { get; set; }
        public Guid VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public IEnumerable<ItemVenda> Itens { get; set; }
    }
}
