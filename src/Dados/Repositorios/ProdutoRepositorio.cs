﻿using Dados.Context;
using Dados.Entidades;
using Dados.Interfaces;

namespace Dados.Repositorios
{
    public class ProdutoRepositorio : Repositorio<Produto>, IProdutoRepositorio
    {
        public ProdutoRepositorio(APIContext db) : base(db)
        {
        }
    }
}
