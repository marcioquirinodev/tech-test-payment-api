﻿using Dados.Context;
using Dados.Entidades;
using Dados.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Dados.Repositorios
{
    public class VendaRepositorio : Repositorio<Venda>, IVendaRepositorio
    {
        public VendaRepositorio(APIContext db) : base(db)
        {
        }
        public override async Task<IEnumerable<Venda>> ObterTodos()
        {
            return await DbSet.AsNoTracking()
                .Include(x=>x.Vendedor)
                .Include(x=>x.Itens)
                .ThenInclude(x=>x.Produto)
                .ToListAsync();
        }
        public override async Task<Venda> ObterPorId(Guid id)
        {
            return await DbSet.AsNoTracking()
                .Include(x => x.Vendedor)
                .Include(x => x.Itens)
                .ThenInclude(x => x.Produto)
                .FirstOrDefaultAsync(x=>x.Id == id);
        }
    }
}
