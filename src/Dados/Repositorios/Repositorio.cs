﻿using Dados.Context;
using Dados.Entidades;
using Dados.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dados.Repositorios
{
    public class Repositorio<T> : IRepositorio<T> where T : Entidade
    {
        protected readonly APIContext Db;
        protected readonly DbSet<T> DbSet;

        public Repositorio(APIContext db)
        {
            Db = db;
            DbSet = db.Set<T>();
        }

        public virtual async Task<bool> Adicionar(T obj)
        {
            DbSet.Add(obj);
            return (await SaveChanges() > 0);
        }

        public virtual async Task<bool> Atualizar(T obj)
        {
            DbSet.Update(obj);
            return (await SaveChanges() > 0);
        }


        public virtual async Task<T> ObterPorId(Guid id)
        {
            return await DbSet.FindAsync(id);
        }

        public virtual async Task<IEnumerable<T>> ObterTodos()
        {
            return await DbSet.ToListAsync();
        }

        public virtual async Task<bool> Remover(Guid id)
        {
            DbSet.Remove(await ObterPorId(id));
            return (await SaveChanges() > 0);
        }

        public async Task<int> SaveChanges()
        {
            return await Db.SaveChangesAsync();
        }
        public void Dispose()
        {
            Db?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
