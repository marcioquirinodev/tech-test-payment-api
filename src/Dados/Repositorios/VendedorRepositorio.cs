﻿using Dados.Context;
using Dados.Entidades;
using Dados.Interfaces;

namespace Dados.Repositorios
{
    public class VendedorRepositorio : Repositorio<Vendedor>, IVendedorRepositorio
    {
        public VendedorRepositorio(APIContext db) : base(db)
        {
        }
    }
}
