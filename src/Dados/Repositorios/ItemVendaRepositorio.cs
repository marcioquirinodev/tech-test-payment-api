﻿using Dados.Context;
using Dados.Entidades;
using Dados.Interfaces;

namespace Dados.Repositorios
{
    public class ItemVendaRepositorio : Repositorio<ItemVenda>, IItemVendaRepositorio
    {
        public ItemVendaRepositorio(APIContext db) : base(db)
        {
        }
    }
}
