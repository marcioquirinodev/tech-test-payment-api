﻿using System.ComponentModel.DataAnnotations;

namespace Dados.Enum
{
    public enum Status
    {
        [Display(Name = "Aguardando Pagamento")]
        AguardandoPagamento,
        [Display(Name = "Pagamento Aprovado")]
        PagamentoAprovado,
        Enviado,
        Entregue,
        Cancelada
    }
}
