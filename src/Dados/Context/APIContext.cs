﻿using Dados.Entidades;
using Microsoft.EntityFrameworkCore;

namespace Dados.Context
{
    public class APIContext : DbContext
    {
        public APIContext(DbContextOptions<APIContext> options) : base(options)
        {
        }
        public DbSet<ItemVenda> Itens { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(APIContext).Assembly);
            base.OnModelCreating(builder);
        }
    }
}
