using Dados.Context;
using Dados.Interfaces;
using Dados.Repositorios;
using Microsoft.EntityFrameworkCore;
using Negocios.Interfaces;
using Negocios.Services;
using System.Text.Json.Serialization;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.ComponentModel;
using Microsoft.EntityFrameworkCore.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<APIContext>(options =>
options.UseSqlServer(builder.Configuration.GetConnectionString("ConexaoPadrao")));

builder.Services.AddControllers();

builder.Services.AddControllers().AddJsonOptions(options =>
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Venda API",
        Description = "API de Vendas de Produtos",
        Contact = new OpenApiContact
        {
            Name = "Marcio Quirino",
            Email = "marcioquirino.dev@outlook.com",
            
        },
        License = new OpenApiLicense
        {
            Name = "MIT",
            Url = new Uri("https://opensource.org/licenses/MIT")
        }
    });
    // using System.Reflection;
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddTransient<IItemVendaRepositorio, ItemVendaRepositorio>();
builder.Services.AddTransient<IProdutoRepositorio, ProdutoRepositorio>();
builder.Services.AddTransient<IVendaRepositorio, VendaRepositorio>();
builder.Services.AddTransient<IVendedorRepositorio, VendedorRepositorio>();

builder.Services.AddTransient<IVendaService, VendaService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseSwaggerUI(c => {
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Vendas API v1");
    c.RoutePrefix = String.Empty;
});
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
