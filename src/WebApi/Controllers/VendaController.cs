﻿using Microsoft.AspNetCore.Mvc;
using Negocios.DTO.Create;
using Negocios.DTO.Update;
using Negocios.Interfaces;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("venda")]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService _vendaService;

        public VendaController(IVendaService vendaService)
        {
            _vendaService = vendaService;
        }

        /// <summary>
        /// Exibe todas as vendas disponíveis
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [Route("/vendas")]
        public async Task<IActionResult> Index()
        {
            return Ok(await _vendaService.BuscarTodasAsVendas());
        }
        /// <summary>
        /// Exibe a venda solicitada por id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:Guid}")]
        public async Task<IActionResult> ObterVendasPorId(Guid id)
        {
            var venda = await _vendaService.BuscarVendaPorId(id);
            if (venda == null) return NotFound();
            return Ok(venda);
        }
        /// <summary>
        /// Cadastra umna nova venda
        /// </summary>
        /// <param name="venda"></param>
        /// <returns></returns>
        [HttpPost()]
        public async Task<IActionResult> CadastrarVenda(VendaCadastroDTO venda)
        {
            if (venda == null) return NotFound();
            if (await _vendaService.Cadastrar(venda)) return Ok(venda);
            return BadRequest();
        }
        /// <summary>
        /// Atualiza o status da Venda
        /// </summary>
        /// <param name="id"></param>
        /// <param name="venda"></param>
        /// <returns></returns>
        [HttpPut("{id:Guid}")]
        public async Task<IActionResult> AtualizarStatusVenda( Guid id, StatusVendaDTO venda)
        {
            if (await _vendaService.AtualizarStatusDaVenda(id, venda)) return Ok(await _vendaService.BuscarVendaPorId(id));

            return BadRequest();
        }
    }
}
