﻿using AutoMapper;
using Dados.Entidades;
using Dados.Enum;
using Dados.Interfaces;
using Negocios.DTO.Create;
using Negocios.DTO.Read;
using Negocios.DTO.Update;
using Negocios.Interfaces;

namespace Negocios.Services
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepositorio _vendaRepositorio;
        private readonly IMapper _mapper;

        public VendaService(IVendaRepositorio vendaRepositorio, IMapper mapper)
        {
            _vendaRepositorio = vendaRepositorio;
            _mapper = mapper;
        }

        public async Task<bool> Cadastrar(VendaCadastroDTO model)
        {
            var venda = _mapper.Map<Venda>(model);

            return await _vendaRepositorio.Adicionar(venda);

        }

        public async Task<IEnumerable<VendaDTO>> BuscarTodasAsVendas()
        {
            return _mapper.Map<IEnumerable<VendaDTO>>(await _vendaRepositorio.ObterTodos());
        }
        public async Task<VendaDTO> BuscarVendaPorId(Guid id)
        {
            return _mapper.Map<VendaDTO>(await _vendaRepositorio.ObterPorId(id));
        }

        public async Task<bool> AtualizarStatusDaVenda(Guid id, StatusVendaDTO model)
        {
            var venda = await _vendaRepositorio.ObterPorId(id);
            if (venda == null) return false;

            if (
                (venda.Status == Status.AguardandoPagamento) &&
                (model.Status == Status.PagamentoAprovado || model.Status == Status.Cancelada)
                )
            {
                venda.Status = model.Status;
            }
            else if (
                (venda.Status == Status.PagamentoAprovado) &&
                (model.Status == Status.Enviado || model.Status == Status.Cancelada)
                )
            {
                venda.Status = model.Status;
            }
            else if (venda.Status == Status.Enviado && model.Status == Status.Entregue)
            {
                venda.Status = model.Status;
            }
            else
            {
                string mensagemErro = @"
A atualização de status deve permitir somente as seguintes transições:
De: Aguardando pagamento  Para: Pagamento Aprovado
De: Aguardando pagamento   Para: Cancelada
De: Pagamento Aprovado    Para: Enviado para Transportadora
De: Pagamento Aprovado   Para: Cancelada
De: Enviado para Transportador. Para: Entregue
";
                throw new Exception(mensagemErro);
            }
            if (await _vendaRepositorio.Atualizar(venda)) return true;
            return false;
        }
    }

}
