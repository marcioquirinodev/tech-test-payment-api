﻿using Negocios.DTO.Create;
using Negocios.DTO.Read;
using Negocios.DTO.Update;

namespace Negocios.Interfaces
{
    public interface IVendaService
    {
        Task<bool> AtualizarStatusDaVenda(Guid id, StatusVendaDTO model);
        Task<IEnumerable<VendaDTO>> BuscarTodasAsVendas();
        Task<VendaDTO> BuscarVendaPorId(Guid id);
        Task<bool> Cadastrar(VendaCadastroDTO model);
    }
}
