﻿using AutoMapper;
using Dados.Entidades;
using Negocios.DTO.Create;
using Negocios.DTO.Read;
using Negocios.DTO.Update;

namespace Negocios.AutoMapper
{
    internal class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<ItemVenda, ItemVendaCadastroDTO>().ReverseMap();
            CreateMap<Produto, ProdutoCadastroDTO>().ReverseMap();
            CreateMap<Venda, VendaCadastroDTO>().ReverseMap();  
            CreateMap<Vendedor, VendedorCadastroDTO>().ReverseMap();

            CreateMap<ItemVenda, ItemVendaDTO>().ReverseMap();
            CreateMap<Produto, ProdutoDTO>().ReverseMap();
            CreateMap<Venda, VendaDTO>().ReverseMap();
            CreateMap<Vendedor, VendedorDTO>().ReverseMap();            
        }
    }
}
