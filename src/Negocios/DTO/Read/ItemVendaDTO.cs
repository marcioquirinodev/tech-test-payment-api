﻿using System.ComponentModel;

namespace Negocios.DTO.Read
{
    public class ItemVendaDTO
    {
        public Guid Id { get; set; }
        public int Quantidade { get; set; }
        public ProdutoDTO Produto { get; set; }
    }
}
