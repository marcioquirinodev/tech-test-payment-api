﻿namespace Negocios.DTO.Read
{
    public class ProdutoDTO
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
    }
}
