﻿using Dados.Enum;

namespace Negocios.DTO.Read
{
    public class VendaDTO
    {
        public Guid Id { get; set; }
        public DateTime Data { get; set; }
        public Status Status { get; set; }
        public VendedorDTO Vendedor { get; set; }
        public IEnumerable<ItemVendaDTO> Itens { get; set; }
    }
}
