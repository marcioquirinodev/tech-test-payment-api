﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace Negocios.DTO.Create
{
    public class VendedorCadastroDTO
    {
        public VendedorCadastroDTO(string cPF, string nome, string email, string telefone)
        {
            CPF = cPF;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }

        [Required(ErrorMessage = "O CPF é obrigatório!")]
        public string CPF { get; set; }
        [Required(ErrorMessage = "O nome é obrigatório!")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "O e-mail é obrigatório!")]
        [EmailAddress(ErrorMessage = "E-mail inválido!")]
        public string Email { get; set; }
        [Required(ErrorMessage = "O telefone é obrigatório!")]
        public string Telefone { get; set; }

    }
}
