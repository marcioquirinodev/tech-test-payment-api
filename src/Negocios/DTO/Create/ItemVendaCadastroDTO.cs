﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Negocios.DTO.Create
{
    public class ItemVendaCadastroDTO
    {
        public ItemVendaCadastroDTO(int quantidade, ProdutoCadastroDTO produto)
        {
            Quantidade = quantidade;
            Produto = produto;
        }

        [Required(ErrorMessage = "A quantidade é obrigatória!")]
        public int Quantidade { get; set; }
        public ProdutoCadastroDTO Produto { get; set; }
    }
}
