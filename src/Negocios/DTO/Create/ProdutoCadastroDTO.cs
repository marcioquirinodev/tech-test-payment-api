﻿using System.ComponentModel.DataAnnotations;

namespace Negocios.DTO.Create
{
    public class ProdutoCadastroDTO
    {
        public ProdutoCadastroDTO(string descricao, decimal valor)
        {
            Descricao = descricao;
            Valor = valor;
        }

        [Required(ErrorMessage = "A descrição é obrigatória!")]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
        [Required(ErrorMessage = "O valor é obrigatório!")]
        public decimal Valor { get; set; }
    }
}
