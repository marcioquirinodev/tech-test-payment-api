﻿using Dados.Enum;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace Negocios.DTO.Create
{
    public class VendaCadastroDTO
    {
        [Required(ErrorMessage = "A data é obrigatória!")]
        public DateTime Data { get; set; }
        [Required(ErrorMessage = "O status é obrigatório!")]
        public Status Status { get; set; }
        [Required(ErrorMessage = "A informações do Vendedor são obrigatórias!")]
        public VendedorCadastroDTO Vendedor { get; set; }
        [Required(ErrorMessage = "Pelo menos um item é obrigatório!")]
        public IEnumerable<ItemVendaCadastroDTO> Itens { get; set; }
    }
}
