﻿using Dados.Enum;

namespace Negocios.DTO.Update
{
    public class StatusVendaDTO
    {
        public Status  Status { get; set; }
    }
}
